# ekart-chrome-extension

A chrome extension which generates a horizontal list of products usingan API fron eLocals.in

## What is a chrome extension

Extensions are small software programs that customize the browsing experience. They enable users to tailor Chrome functionality and behavior to individual needs or preferences. They are built on web technologies such as HTML, JavaScript, and CSS.

## How to use this extension

1. Download and extract the project repository into your local computer.

2. Open chrome browser and click on menu icon on the top-right corner of the browser.

3. Go to " More Tools -> Extensions "

4. Switch on the Developer mode option

5. Click on "Load Unpacked"

6. Select the project folder you downloaded and extracted onto your computer.

7. You will get an extension icon on the right of the omni-search-box

8. Click on the extension icon to run it.